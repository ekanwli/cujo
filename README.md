

### Description:
Simple REST API to provide service_category of a subscriber using subscriber MSISDN by fetching it from Memcached.
This suite contains docker images for
- Actual Application exposed through Nginx
- Test application to populate the memcache and then testing REST API to measure end-to-end latency.
- Deployment suite -- Top level scripts to deploy memcached, application and tests on AWS EC2 instances.

### Getting Started

To deploy and run the application, we just need to build and start the deployment container on any machine.

#### Pre-requisites:
- Clone the repository
```
git clone git@bitbucket.org:ekanwli/cujo.git
```
- AWS Access credentials: Access credentials are required by AWS API to deploy the application. The AWS user should have permission to manage EC2 instances.
```
export AWS_ACCESS_KEY_ID=<Your AWS Access Key ID>
export ENV AWS_SECRET_ACCESS_KEY=<Your AWS Secret Access Key>
```
- Docker should be installed on the machine which will run the deployment container.

#### Build
- Change directory to repository root directory
```
cd cujo
```
- Build Provisioner Docker image
```
docker build -t provisioner .
```

### Run
Start the provisioner container. It will take a while for some steps to process. Please be patient. If some step hangs for more that 2 minutes then that is an anomally.
```
docker run -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} provisioner
```

### Provisioner:
The provisoner container will perform the following steps:

- Remove any previously running AWS EC2 instances.
- Create new Key pair for EC2 instances
- Create new security group for allowing ingress SSH, HTTP and Memcached ports.
- Start 3 EC2 instances.
- Install Docker on each instance
- Send Dockerfiles for the application and tests to each machine respectively.
- Build Docker images from dockerfile on target EC2 instances.
- Start the following on each EC2 instance respectively:
    - Memcached container
    - Application container exposed by Nginx
    - Test container
- You will see the test results when all tests are finished like the following:

```
...
...
Starting hit latency test. Iterations = 2000
Starting miss latency test. Iterations = 2000
=== RESULTS ===
Average HIT Latency  : 236.963000 ms
Average MISS Latency : 391.907000 ms

```
**Note**:
> The provisoner script performs the cleanup while starting so no manual intervention is required for running the script again.

**Note**:
> In case of some error, You can enable Logging for provisoner script by setting ```ENABLE_PEXPECT_LOGS = True``` in ```provision/app/main.py``` and perform the build and run step again for provisioner

##### Possible Improvements:
- Instead of manually creating EC2 instances. The application containers can the deployed on Kubernetes. That will require extra steps for creating kubernetes objects like deployments and services etc.
- Instead of using single script, ansible playbooks can be written for the whole provisoning process.
- The scripts can be configured to spawn more instances for horizontal scaling of application or memcached.

### Memcached:
The official docker container is used to deploy memecached. By default, maximum of 4 GB memory is allocated for memcached by provisoner. More instances of memcached can be deployed in parallel to increase the overall memory but more planning is required from the client side to distribute the load and synchronizing the load distribution with all clients. Also, how to perform live addition or removal of nodes.

### Application:
The application container contains the application itself exposed via Nginx. Since, the application's REST API is fairly simple so we don't need any complex web frameworks for that. I have used flask for REST API, which is a light python framework for handling web requests. Flask application is listening requests through WSGI interface. Nginx proxies all requests to WSGI interface.

Following REST endpoints are implemented.

| Endpoint               | Description |
|------------------------|-------------|
| ```/subscriber/<msisdn>```   | Returns service category list in JSON format.
| ```/status```| Return helth status of application. Return code 200 tells whether application is up and successfully connected to memcached. Any other code implies error or not ready.

##### Design Thoughts:
Initially I was trying to make a sophisticated scheme for storing and accessing subscriber data. But I found that memcache is also using hash tables to store data and it is also using one the fastest hash functions (Murmer 3) in the market. The design I thought was very much like memcache itself. So instead of redoing it, I stored all data in memcache and just access it when requested.

**Note**:
> Using memcache for long term storage is not a preffered way. It is designed to act like a cache and the main data storage should be somewhere in a real database. It can slow the system when there is a cache miss.

**Note**:
> I checked the performance impact of very huge cache to check whether it will impact the latency. I tried to find the latency with 1000 subscribers and compared it with 1 Billion subscribers on a single memcached node by providing 24GB Memory to memcached. Both results were really close. So, increasing the keys(atleast to 10^9) in memcached does not impact the latency very much.

**Note**:
> I used pymemcache as a memcached client. One thing I noticed is that if you provide DNS addresses for memcached server, increases the latency to several thousand folds. Even though I tried to resolve the DNS before starting the tests but there was no improvement. I didn't had time to isolate this issue. I could be problem with the client or some incomplete DNS configuration in the docker daemon. Anyway, I finally used the plain IP addresses for accessing memcached. Those IP addresses are extracted by provisioner script and injected in the application container as environment variables.

Source code for application and its dockerfile is available in ```app/``` directory.

##### Possible Improvements:
- input sanitization
- Add more documentation
- Write unit-tests
- Improve debugging
- Distributed tracing -- if the application is part of a bigger cluster for enhanced logging, debugging and analysis.
- Improved error handling

### Tests:

The test container runs a single python script which will generate a series of MSISDN numbers, assigns random service categories with each msisdn and stores directly in memcached. Once all data is populated, It accesses the application using REST insterface and take average latency from 2000 requests.


Source code is located in ```test``` directory

### Results:

I have ran the the containers on my local machine(Intel core i7 - 8th gen) as well and on t2.micro EC2 instances. The average HIT latencies varies between 200ms and 450ms. It doesnt matter how big the cache size is. IMO, some 20%-30% of these latencies is coming from network operations and web request handling.



