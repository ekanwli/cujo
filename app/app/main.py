import os
from flask import Flask
from flask_restful import Api
from subscriber import Subscriber, Status
from memcache import Memcache


def init_memcache_client():
    # Initialize memcache client
    # example env: MEMCACHED_SERVERS="127.0.0.1:11211,127.0.0.2:12345"
    server_cfg = os.environ.get('MEMCACHED_SERVERS')
    if server_cfg is None:
        default_memcache = [('localhost', 11211)]
        memcache_servers = default_memcache
    else:
        nodes = server_cfg.split(',')
        memcache_servers = []
        for n in nodes:
            host, port = n.split(':')
            memcache_servers.append((host, int(port)))

    print("memcache servers = " + str(memcache_servers))
    Memcache(memcache_servers)


init_memcache_client()
app = Flask(__name__)
api = Api(app)
api.add_resource(Subscriber, "/subscriber/<string:msisdn>")
api.add_resource(Status, "/status")
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8080)
