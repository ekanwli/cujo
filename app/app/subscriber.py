from flask_restful import Resource, reqparse
from memcache import Memcache
import json


class Subscriber(Resource):
    def get(self, msisdn):

        # TODO: sanitize msisdn

        client = Memcache().client()
        data = client.get(msisdn)
        if data:
            return json.dumps(data), 200
        else:
            return "Data Not Found", 404


class Status(Resource):
    def get(self):

        try:
            client = Memcache().client()
            data = 'test-data'
            client.set('test-key', data)
            cache_data = client.get('test-key')
            if data == str(cache_data, 'utf8'):
                return "System is up and running", 200
            else:
                return "Failed to set/get data from memcache", 500
        except Exception as e:
            return "Failed to set/get data from memcache", 500
