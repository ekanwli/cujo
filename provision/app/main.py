import sys
import boto3
from pathlib import Path
import time
import pexpect
from pexpect import pxssh

# Script global config
IMAGEID = 'ami-c7e0c82c'
KEYNAME = 'memcache-test-key-pair'
SECURITY_GROUP_NAME = 'memcache-test-sec-group'
FILTER_TAG = 'CUJO'
ENABLE_PEXPECT_LOGS = False
ec2 = boto3.resource('ec2')
client = boto3.client('ec2')
KEY_PATH = ''
IMAGE_USERNAME = 'ubuntu'
EXPECT_LOG = sys.stdout if ENABLE_PEXPECT_LOGS else None


def remove_old_instances():
    """
    Remove any previously created instances(if any)
    """
    old_inst = client.describe_instances(Filters=[
                                             {
                                                 'Name': 'tag-key',
                                                 'Values': [
                                                     FILTER_TAG,
                                                 ]
                                             },
                                         ])

    insts = []
    for res in old_inst.get('Reservations'):
        for inst in res.get('Instances'):
            i = ec2.Instance(inst.get('InstanceId'))
            if (i.state.get('Code') & 0xff) != 48:
                i.terminate()
                insts.append(i)

    for i in insts:
        print("Waiting for %s to terminate" % str(i))
        i.wait_until_terminated()


def gen_key_pair():
    """
    create key pair and save to file
    """
    try:
        client.delete_key_pair(KeyName=KEYNAME)
    except Exception as e:
            pass

    keypair = client.create_key_pair(KeyName=KEYNAME)
    localkeyfilepath = Path('/provision').joinpath(KEYNAME + '.pem')
    localkeyfilepath.write_text(keypair['KeyMaterial'])
    localkeyfilepath.chmod(0o600)
    print(keypair['KeyMaterial'])
    return keypair, localkeyfilepath


def create_security_group():
    """
    Deletes old security group(if any) then creates new one for SSH,
    HTTP and MEMCACHED
    """
    try:
        client.delete_security_group(GroupName=SECURITY_GROUP_NAME)
    except Exception as e:
            pass

    secgrp = ec2.create_security_group(GroupName=SECURITY_GROUP_NAME,
                                      Description="Open access for SSH, HTTP and memcached")
    secgrp.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=80,ToPort=80)
    secgrp.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=22,ToPort=22)
    secgrp.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=11211,ToPort=11211)


def launch_instance():
    """
    Launches EC2 instance from ubuntu 16.04 AMI and assigns global attributes
    """
    tag_spec = [
                   {
                       'ResourceType': 'instance',
                       'Tags': [
                           {
                               'Key': FILTER_TAG,
                               'Value': 'True'
                           },
                       ]
                   },
               ]
    inst = ec2.create_instances(ImageId=IMAGEID,
                                InstanceType='t2.micro',
                                KeyName=KEYNAME,
                                MinCount=1, MaxCount=1,
                                SecurityGroups=[SECURITY_GROUP_NAME],
                                TagSpecifications=tag_spec)

    return inst




# connect to instance and initialize
def install_docker(host):
    username = IMAGE_USERNAME
    key = KEY_PATH
    time.sleep(10)
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},
                        logfile=EXPECT_LOG,
                        encoding='utf-8',
                        echo=False)
        s.login(host, username, ssh_key=key)
        s.sendline("sudo apt-get update; "
                   "sudo apt-get install -y apt-transport-https "
                   "                        ca-certificates "
                   "                        curl "
                   "                        software-properties-common; "
                   "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -; "
                   "sudo add-apt-repository -y "
                   "  \"deb [arch=amd64] https://download.docker.com/linux/ubuntu "
                   "    $(lsb_release -cs) "
                   "    stable\"; "
                   "sudo apt-get update; "
                   "sudo apt-get install -y docker-ce ;"
                   "sudo usermod -aG docker $USER ;"
                   "sudo mkdir /data"
                   "sudo chown -R $USER:$USER /data"
                   )
        s.prompt()             # match the prompt

        s.logout()

    except pxssh.ExceptionPxssh as e:
        print("Failed to install Docker on target machines")
        print(e)


def send_dockerfile(src, dst, host):
    username = IMAGE_USERNAME
    key = KEY_PATH
    try:
        scp_cmd = "/usr/bin/scp \
                        -o StrictHostKeyChecking=no \
                        -o UserKnownHostsFile=/dev/null \
                        -i %s \
                        -r %s \
                        %s@%s:%s" % (key, src, username, host, dst)
        p = pexpect.spawn(scp_cmd,
                          echo=False,
                          logfile=EXPECT_LOG,
                          encoding='utf-8')
        p.expect(pexpect.EOF)

    except Exception as e:
        print("Error occurred while sending Dockerfiles to target machine")
        print(e)


def build_dockerfile(path, tag, host):
    username = IMAGE_USERNAME
    key = KEY_PATH
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},
                        logfile=EXPECT_LOG,
                        encoding='utf-8',
                        echo=False)
        s.login(host, username, ssh_key=key)

        build_cmd = "cd %s; docker build -t %s ." % (path, tag)
        s.sendline(build_cmd)   # run a command
        s.prompt()             # match the prompt

        s.logout()
    except pxssh.ExceptionPxssh as e:
        print("Dockerfile build failed")
        print(e)


def start_container(image, opts, cmd, host):
    username = IMAGE_USERNAME
    key = KEY_PATH
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},
                        logfile=EXPECT_LOG,
                        encoding='utf-8',
                        echo=False)
        s.login(host, username, ssh_key=key)

        run_cmd = "docker run %s %s %s" % (opts, image, cmd)
        s.sendline(run_cmd)   # run a command
        s.prompt()             # match the prompt

        s.logout()
    except pxssh.ExceptionPxssh as e:
        print("Failed to start container")
        print(e)


def main():
    print('Removing any old instances to free resources')
    remove_old_instances()
    print('(Re)generate Key/Pair')
    keypair, localkeyfilepath = gen_key_pair()
    global KEY_PATH
    KEY_PATH = str(localkeyfilepath)
    print('(Re)create Security Group')
    create_security_group()

    print('Launching EC2 instances')
    memcache_inst = launch_instance()
    app_inst = launch_instance()
    test_inst = launch_instance()

    print("Waiting for following instances to launch:")
    print(memcache_inst[0])
    print(app_inst[0])
    print(test_inst[0])
    memcache_inst[0].wait_until_running()
    app_inst[0].wait_until_running()
    test_inst[0].wait_until_running()

    # get instance info (ip, etc.)
    memcache_addr = ec2.Instance(memcache_inst[0].id).public_dns_name
    memcache_priv_addr = ec2.Instance(memcache_inst[0].id).private_ip_address
    app_addr = ec2.Instance(app_inst[0].id).public_dns_name
    app_priv_addr = ec2.Instance(app_inst[0].id).private_ip_address
    test_addr = ec2.Instance(test_inst[0].id).public_dns_name
    print("Memcache address = %s" % memcache_addr)
    print("Memcache private address = %s" % memcache_priv_addr)
    print("App address = %s" % app_addr)
    print("App private address = %s" % app_priv_addr)
    print("Test address = %s" % test_addr)

    # Install Docker on new images
    print('Installing Docker on newly spawned instances')
    install_docker(memcache_addr)
    install_docker(app_addr)
    install_docker(test_addr)

    # Send dockerfiles to each Machine
    print('Sending Dockerfiles to individual instances to build and launch')
    send_dockerfile('/app', '~/data', app_addr)
    send_dockerfile('/test', '~/data', test_addr)

    # Build Dockerfiles on targets
    print('Building Docker images on targets')
    build_dockerfile('~/data', 'app:latest', app_addr)
    build_dockerfile('~/data', 'test:latest', test_addr)

    # launch containers on instance
    print('Start Memcached container')
    start_container('memcached',
                    '--rm -d -p 11211:11211',
                    'memcached -m 4000 -p 11211 -U 0',
                    memcache_addr)
    print('Start application container')
    start_container('app:latest',
                    "--rm -d -p 80:80 -e MEMCACHED_SERVERS='%s:11211'" % (memcache_priv_addr),
                    '',
                    app_addr)
    print('Start test container for populating data and measure latency')

    global EXPECT_LOG
    EXPECT_LOG = sys.stdout
    start_container('test:latest',
                    "--rm -e MEMCACHED_SERVERS='%s:11211' -e 'APP_URL=http://%s'" % (memcache_priv_addr,
                                                                                     app_priv_addr),
                    '',
                    test_addr)

if __name__ == "__main__":
    main()
