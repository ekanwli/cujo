FROM python:3.6-alpine3.7

RUN apk update \
    && apk add --no-cache --virtual openssh-client \
    && rm -rf /var/lib/apt/lists/* \
              /var/cache/apk/* \
              /usr/share/man \
              /tmp/*
RUN pip install awscli --upgrade --user
RUN pip install boto3
RUN pip install pexpect

# AWS API configuration
#ENV AWS_ACCESS_KEY_ID=AKIAIRCON3M65MSR7ZPQ
#ENV AWS_SECRET_ACCESS_KEY=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
ENV AWS_DEFAULT_REGION=eu-central-1

# Copy applications to launch
COPY app /app/
COPY provision /provision/
COPY test /test/
WORKDIR /data

CMD python -u /provision/app/main.py
