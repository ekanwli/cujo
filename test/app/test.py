import os
import time
import requests
from threading import Thread
from memcache import Memcache
import datetime
import math


MAX_CLIENT_EXP = 15
NUM_THREADS = 1
STEP = 2**(MAX_CLIENT_EXP - int(math.log(NUM_THREADS)/math.log(2)))
BASE_NUM = 123_456_789

client = None   # memcached client object
APP_URL = 'http://localhost'  # Address where applicaton is running.


def init_memcache_client():
    # Initialize memcache client
    # example env: MEMCACHED_SERVERS="127.0.0.1:11211,127.0.0.2:12345"
    server_cfg = os.environ.get('MEMCACHED_SERVERS')
    if server_cfg is None:
        default_memcache = [('localhost', 11211)]
        memcache_servers = default_memcache
    else:
        nodes = server_cfg.split(',')
        memcache_servers = []
        for n in nodes:
            host, port = n.split(':')
            memcache_servers.append((host, int(port)))
    print("memcache servers = " + str(memcache_servers))
    Memcache(memcache_servers)


def populate_msisdn(start, stop):
    for i in range(start, stop):
        # generate msisdn
        msisdn = '+358' + str(BASE_NUM + i)
        # random data for each msisdn
        data = {'srv_cat': [i, i+1, i+2, i+3, i+4, i+5]}
        # insert data in memcached
        client.set(msisdn, data)

    print("Subscribers added successfully. [%d, %d]" % (start, stop))


def wait_for_app():
    host = APP_URL + '/status'
    print("Waiting for app[%s]" % host)

    # Retry for 300 seconds to see whether app is up or not
    counter_sec = 300
    while counter_sec > 0:
        code = requests.get(host, verify=False).status_code
        if code == 200:
            break
        time.sleep(1)
        counter_sec -= 1
        print('Retrying: %s sec left' % counter_sec)

    if counter_sec == 0:
        print('App Failed to load in specified time')
    else:
        print('App loaded successfully')


def test_hit_latency():
    host = APP_URL + '/subscriber/+358'
    num_requests = 2000 if MAX_CLIENT_EXP >= 12 else 2**MAX_CLIENT_EXP
    print('Starting hit latency test. Iterations = %d' % num_requests)

    start_time = datetime.datetime.now()
    for i in range(num_requests):
        host_i = host + str(BASE_NUM + i)
        requests.get(host_i, verify=False).status_code

    end_time = datetime.datetime.now()
    diff = end_time - start_time
    diff_msec = diff.seconds * 1000 + diff.microseconds
    latency = diff_msec / num_requests
    return latency

    print("Hit Latency = %f ms" % latency)


def test_miss_latency():
    host = APP_URL + '/subscriber/+001'
    num_requests = 2000 if MAX_CLIENT_EXP >= 12 else 2**MAX_CLIENT_EXP
    print('Starting miss latency test. Iterations = %d' % num_requests)

    start_time = datetime.datetime.now()
    for i in range(num_requests):
        host_i = host + str(i)
        requests.get(host_i, verify=False).status_code

    end_time = datetime.datetime.now()
    diff = end_time - start_time
    diff_msec = diff.seconds * 1000 + diff.microseconds
    latency = diff_msec / num_requests
    return latency


if __name__ == "__main__":
    init_memcache_client()
    client = Memcache().client()

    print("Populating %d subscribers in memcached" % (2**MAX_CLIENT_EXP))
    # Start threads to populate msisdn data in cache
    threads = []
    for i in range(NUM_THREADS):
        rstart = STEP * i
        rstop = rstart + STEP
        t = Thread(target=populate_msisdn, args=(rstart, rstop))
        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    # Get app url from env
    url = os.environ.get('APP_URL')
    if url is not None:
        APP_URL = url

    # Wait for the app to start
    wait_for_app()

    # Test hit latency
    hit_latency = test_hit_latency()

    # test miss latency
    miss_latency = test_miss_latency()
    print("=== RESULTS ===")
    print("Average HIT Latency  : %f ms" % hit_latency)
    print("Average MISS Latency : %f ms" % miss_latency)
