import json
from pymemcache.client.hash import HashClient


def json_serializer(key, value):
    if type(value) == str:
        return value, 1
    return json.dumps(value), 2


def json_deserializer(key, value, flags):
    if flags == 1:
        return value
    if flags == 2:
        return json.loads(value)
    raise Exception("Unknown serialization format")


class Memcache:
    class __Memcache:
        def __init__(self, servers):
            self.servers = servers
            self.client = HashClient(servers,
                                     serializer=json_serializer,
                                     deserializer=json_deserializer)

        def __str__(self):
            return repr(self) + str(self.servers)
    instance = None

    def __init__(self, servers=[]):
        if not Memcache.instance:
            Memcache.instance = Memcache.__Memcache(servers)
        else:
            # Configure only once
            pass

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def client(self):
        return self.instance.client
